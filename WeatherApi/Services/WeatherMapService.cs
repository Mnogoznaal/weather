﻿using System.IO;
using System.Text.Json;
using WeatherApi.Repository.Interfaces;
using WeatherApi.Services.Interfaces;

namespace WeatherApi.Services
{
    public class WeatherMapService : IWeatherMapService
    {
        private readonly IWeatherRepository _weatherRepository;
        private readonly IConfiguration _configuration;
        public WeatherMapService(IWeatherRepository weatherRepository, IConfiguration configuration)
        {
            _weatherRepository = weatherRepository;
            _configuration = configuration;
        }
        private static HttpClient client = new HttpClient();
        public async Task<Weather> GetWeatherByGeo(string latitude, string longitude)
        {
            Weather? weather = null;
            string token = _configuration.GetValue<string>("WeatherToken");
            HttpResponseMessage response = await client.GetAsync($"https://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={longitude}&appid={token}");
            if (response.IsSuccessStatusCode)
            {
                string json = await response.Content.ReadAsStringAsync();
                weather = JsonSerializer.Deserialize<Weather>(json);
            }
            await _weatherRepository.Create(weather);
            return weather;
        }

        public async Task<List<Weather>> GetAll()
        {
            return await _weatherRepository.GetAll();
        }
    }
}
