﻿namespace WeatherApi.Services.Interfaces
{
    public interface IWeatherMapService
    {
        Task<Weather> GetWeatherByGeo(string latitude, string longitude);
        Task<List<Weather>> GetAll();
    }
}
