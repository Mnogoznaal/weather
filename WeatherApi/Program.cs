using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Swashbuckle.AspNetCore.SwaggerGen;
using WeatherApi.Middleware;
using WeatherApi.Repository;
using WeatherApi.Repository.Interfaces;
using WeatherApi.Services;
using WeatherApi.Services.Interfaces;
using WeatherApi.Swagger;

var builder = WebApplication.CreateBuilder(args);

string connection = builder.Configuration.GetConnectionString("MongoDbConnection");
string dbname = builder.Configuration.GetConnectionString("DatabaseName");
builder.Services.AddSingleton<IMongoClient>(new MongoClient(connection));
builder.Services.AddSingleton<IMongoDatabase>(provider =>
{
    var client = provider.GetRequiredService<IMongoClient>();
    return client.GetDatabase(dbname);
});
// Add services to the container.
builder.Services.AddScoped<IWeatherRepository, WeatherRepository>();
builder.Services.AddScoped<IWeatherMapService, WeatherMapService>();
builder.Services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseMiddleware<TokenAuthorizationMiddleware>();
app.UseAuthorization();

app.MapControllers();

app.Run();
