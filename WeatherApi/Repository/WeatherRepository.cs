﻿using MongoDB.Driver;
using WeatherApi.Repository.Interfaces;

namespace WeatherApi.Repository
{
    public class WeatherRepository : IWeatherRepository
    {
        private readonly IMongoCollection<Weather> _weatherCollection;
        public WeatherRepository(IMongoDatabase database)
        {
            _weatherCollection = database.GetCollection<Weather>("Weather");
        }
        public async Task<bool> Create(Weather weather)
        {
            try
            {
                await _weatherCollection.InsertOneAsync(weather);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<List<Weather>> GetAll()
        {
            return await _weatherCollection.AsQueryable().ToListAsync();
        }
    }
}
