﻿namespace WeatherApi.Repository.Interfaces
{
    public interface IWeatherRepository
    {
        Task<bool> Create(Weather weather);
        Task<List<Weather>> GetAll();
    }
}
