using Microsoft.AspNetCore.Mvc;
using WeatherApi.Services.Interfaces;

namespace WeatherApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherMapController : ControllerBase
    {
        private readonly IWeatherMapService _weatherMapService;
        public WeatherMapController(IWeatherMapService weatherMapService)
        {
            _weatherMapService = weatherMapService;
        }

        [HttpGet("GetWeather", Name = "GetWeather")]
        public async Task<ActionResult<Weather>> Get(string latitude, string longitude)
        {
            Weather weather = await _weatherMapService.GetWeatherByGeo(latitude, longitude);
            return Ok(weather);
        }

        [HttpGet("GetAll", Name = "GetAll")]
        public async Task<ActionResult<List<Weather>>> GetAll()
        {
            List<Weather> weathers = await _weatherMapService.GetAll();
            return Ok(weathers);
        }
    }
}